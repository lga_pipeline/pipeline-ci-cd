package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.controllers;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.CommentaireDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.DateSondageDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.SondageDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Commentaire;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.CommentaireService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondageService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondeeService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.SondageService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/sondage")
public class SondageController {

    private final SondageService service;
    private final CommentaireService scommentaire;
    private final DateSondageService sdate;
    private final DateSondeeService request;
    private final ModelMapper mapper;

    Logger logger = LoggerFactory.getLogger(SondageController.class);

    public SondageController(SondageService service, ModelMapper mapper, CommentaireService c, DateSondageService d, DateSondeeService r) {
        this.service = service;
        this.mapper = mapper;
        this.sdate = d;
        this.scommentaire = c;
        this.request = r;
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<SondageDto> get(@PathVariable("id") Long id) {
        try {
            var model = service.getById(id);
            if (model == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            return ResponseEntity.status(HttpStatus.OK).body(mapper.map(model, SondageDto.class));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping(value = "/{id}/best")
    @ResponseBody
    public ResponseEntity<List<Date>> getBest(@PathVariable("id") Long id) {
        try {
            var bestDates = request.bestDate(id);
            if (bestDates.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            }
            return ResponseEntity.status(HttpStatus.OK).body(bestDates);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping(value = "/{id}/maybe")
    @ResponseBody
    public ResponseEntity<List<Date>> getMaybeBest(@PathVariable("id") Long id) {
        try {
            var maybeBestDates = request.maybeBestDate(id);
            if (maybeBestDates.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            }
            return ResponseEntity.status(HttpStatus.OK).body(maybeBestDates);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<SondageDto>> get() {
        try {
            var models = service.getAll();
            if (models.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            }
            return ResponseEntity.status(HttpStatus.OK).body(models.stream().map(m -> mapper.map(m, SondageDto.class)).collect(Collectors.toList()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping(value = "/{id}/commentaires")
    @ResponseBody
    public ResponseEntity<List<CommentaireDto>> getCommentaires(@PathVariable("id") Long id) {
        try {
            var models = scommentaire.getBySondageId(id);
            if (models.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            }
            return ResponseEntity.status(HttpStatus.OK).body(models.stream()
                    .map(model -> mapper.map(model, CommentaireDto.class))
                    .collect(Collectors.toList()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping(value = "/{id}/dates")
    @ResponseBody
    public ResponseEntity<List<DateSondageDto>> getDates(@PathVariable("id") Long id) {
        try {
            var models = sdate.getBySondageId(id);
            if (models.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
            }
            return ResponseEntity.status(HttpStatus.OK).body(models.stream()
                    .map(model -> mapper.map(model, DateSondageDto.class))
                    .collect(Collectors.toList()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

        }
    }

    @PostMapping(value = "/")
    @ResponseBody
    public ResponseEntity<SondageDto> create(@RequestBody SondageDto sondageDto) {
        try {
            var model = mapper.map(sondageDto, Sondage.class);
            var result = service.create(sondageDto.getCreateBy(), model);
            if (result == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            return ResponseEntity.status(HttpStatus.CREATED).body(mapper.map(result, SondageDto.class));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping(value = "/{id}/commentaires")
    @ResponseBody
    public ResponseEntity<CommentaireDto> createCommantaire(@PathVariable("id") Long id, @RequestBody CommentaireDto commantaireDto) {
        try {
            var model = mapper.map(commantaireDto, Commentaire.class);
            var result = scommentaire.addCommantaire(id, commantaireDto.getParticipant(), model);
            if (result == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            return ResponseEntity.status(HttpStatus.CREATED).body(mapper.map(result, CommentaireDto.class));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping(value = "/{id}/dates")
    @ResponseBody
    public ResponseEntity<DateSondageDto> createDate(@PathVariable("id") Long id, @RequestBody DateSondageDto dto) {
        try {
            var model = mapper.map(dto, DateSondage.class);
            var result = sdate.create(id, model);
            if (result == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            return ResponseEntity.status(HttpStatus.CREATED).body(mapper.map(result, DateSondageDto.class));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<SondageDto> update(@PathVariable("id") Long id, @RequestBody SondageDto sondageDto) {
        try {
            var model = mapper.map(sondageDto, Sondage.class);
            var result = service.update(id, model);
            if (result == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
            return ResponseEntity.status(HttpStatus.OK).body(mapper.map(result, SondageDto.class));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<SondageDto> delete(@PathVariable("id") Long id) {
        try {
            if (service.delete(id) == 0) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
