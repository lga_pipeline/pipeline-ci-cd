package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondee;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.DateSondeeRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class DateSondeeService {

    private final DateSondeeRepository repository;
    private final DateSondageService dateSondageService;
    private final ParticipantService participantService;

    public DateSondeeService(DateSondeeRepository repository, DateSondageService d, ParticipantService p) {
        this.repository = repository;
        this.dateSondageService = d;
        this.participantService = p;
    }

    @Transactional
    public DateSondee create(Long id, Long participantId, DateSondee dateSondee) {
        DateSondage date = dateSondageService.getById(id);
        Participant participant = participantService.getById(participantId);
        if (date == null || participant == null || dateSondee == null) {
            return null;
        }
        if (Boolean.FALSE.equals(date.getSondage().getCloture())) {
            dateSondee.setDateSondage(date);
            dateSondee.setParticipant(participant);
            return repository.save(dateSondee);
        }
        return null;
    }

    public List<Date> bestDate(Long id) {
        return repository.bestDate(id);
    }

    public List<Date> maybeBestDate(Long id) {
        return repository.maybeBestDate(id);
    }
}
