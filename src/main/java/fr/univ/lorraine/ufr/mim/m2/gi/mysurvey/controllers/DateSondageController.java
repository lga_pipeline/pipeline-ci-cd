package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.controllers;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.DateSondageDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.DateSondeeDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondee;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondageService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondeeService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/date")
public class DateSondageController {

    private final DateSondageService service;
    private final DateSondeeService sds;
    private final ModelMapper mapper;

    public DateSondageController(DateSondageService service, ModelMapper mapper, DateSondeeService s) {
        this.service = service;
        this.mapper = mapper;
        this.sds = s;
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<DateSondageDto> delete(@PathVariable("id") Long id) {
        try {
            if (service.delete(id) == 0) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping(value = "/{id}/participer")
    @ResponseBody
    public ResponseEntity<DateSondageDto> createParticipation(@PathVariable("id") Long id, @RequestBody DateSondeeDto dto) {
        try {
            var model = mapper.map(dto, DateSondee.class);
            var result = sds.create(id, dto.getParticipant(), model);
            if (result == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            return ResponseEntity.status(HttpStatus.CREATED).body(mapper.map(service.getById(id), DateSondageDto.class));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
