package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.DateSondeeDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondee;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondageService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DateSondageController.class)
class DateSondageControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    DateSondageService dateSondageService;

    @MockBean
    DateSondeeService dateSondeeService;

    @MockBean
    ModelMapper modelMapper;

    ObjectMapper objectMapper = new ObjectMapper();

    private DateSondee dateSondee;
    private DateSondeeDto dateSondeeDto;
    private String dateSondeeDtoJson;

    @BeforeEach
    void setUp() throws JsonProcessingException {
        dateSondee = new DateSondee();
        dateSondeeDto = new DateSondeeDto();
        dateSondeeDto.setParticipant(1L);
        dateSondeeDtoJson = objectMapper.writeValueAsString(dateSondeeDto);

    }

    @Test
    void whenCreateParticipation_withValidParameters_thenOK() throws Exception {
        when(modelMapper.map(dateSondeeDto, DateSondee.class)).thenReturn(dateSondee);
        when(dateSondeeService.create(1L, 1L, dateSondee)).thenReturn(dateSondee);
        when(modelMapper.map(dateSondee, DateSondeeDto.class)).thenReturn(dateSondeeDto);

        mockMvc.perform(post("/api/date/1/participer")
                        .content(dateSondeeDtoJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void whenCreateParticipation_withInvalidParameters_thenNotFound() throws Exception {
        when(modelMapper.map(dateSondeeDto, DateSondee.class)).thenReturn(dateSondee);
        when(dateSondeeService.create(1L, 1L, dateSondee)).thenReturn(null);

        mockMvc.perform(post("/api/date/1/participer")
                        .content(dateSondeeDtoJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenCreateParticipation_withInternalError_thenInternalServerError() throws Exception {
        when(modelMapper.map(dateSondeeDto, DateSondee.class)).thenReturn(dateSondee);
        when(dateSondeeService.create(1L, 1L, dateSondee)).thenThrow(new RuntimeException());

        mockMvc.perform(post("/api/date/1/participer")
                        .content(dateSondeeDtoJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenCreateParticipation_withoutRequestBody_thenBadRequest() throws Exception {
        mockMvc.perform(post("/api/date/1/participer"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenDelete_withValidParameters_thenOK() throws Exception {
        when(dateSondageService.delete(anyLong())).thenReturn(1);

        mockMvc.perform(delete("/api/date/1"))
                .andExpect(status().isOk());
    }

    @Test
    void whenDelete_withInvalidParameters_thenNotFound() throws Exception {
        when(dateSondageService.delete(anyLong())).thenReturn(0);

        mockMvc.perform(delete("/api/date/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenDelete_withInternalError_thenInternalServerError() throws Exception {
        when(dateSondageService.delete(anyLong())).thenThrow(new RuntimeException());

        mockMvc.perform(delete("/api/date/1"))
                .andExpect(status().isInternalServerError());
    }

}
