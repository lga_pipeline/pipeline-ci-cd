package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

class ParticipantTest {

    private Participant participant;
    private Participant participant2;

    @BeforeEach
    void setUp() {
        participant = new Participant();
        participant2 = new Participant(1L, "nom", "prenom");
    }

    @Test
    void constructorTest() {
        //Test empty constructor
        Assertions.assertNull(participant.getParticipantId());
        Assertions.assertNull(participant.getNom());
        Assertions.assertNull(participant.getPrenom());
        Assertions.assertTrue(participant.getCommentaire().isEmpty());
        Assertions.assertTrue(participant.getSondages().isEmpty());
        Assertions.assertTrue(participant.getDateSondee().isEmpty());

        //Test constructor with parameters
        Assertions.assertEquals(1L, participant2.getParticipantId());
        Assertions.assertEquals("nom", participant2.getNom());
        Assertions.assertEquals("prenom", participant2.getPrenom());
        Assertions.assertTrue(participant.getCommentaire().isEmpty());
        Assertions.assertTrue(participant.getSondages().isEmpty());
        Assertions.assertTrue(participant.getDateSondee().isEmpty());
    }

    @Test
    void gettersAndSettersTest() {
        participant.setParticipantId(1L);
        participant.setNom("nom");
        participant.setPrenom("prenom");
        participant.setCommentaire(null);
        participant.setSondages(null);
        participant.setDateSondee(null);

        Assertions.assertEquals(1L, participant.getParticipantId());
        Assertions.assertEquals("nom", participant.getNom());
        Assertions.assertEquals("prenom", participant.getPrenom());
        Assertions.assertNull(participant.getCommentaire());
        Assertions.assertNull(participant.getSondages());
        Assertions.assertNull(participant.getDateSondee());
    }

    @Test
    void equalsTest() {
        Assertions.assertEquals(participant, participant);

        Participant participant3 = new Participant(1L, "nom", "prenom");
        participant3.setParticipantId(2L);
        Assertions.assertNotEquals(participant2, participant3);

        Participant participant4 = new Participant(1L, "nom", "prenom");
        participant4.setNom("nom2");
        Assertions.assertNotEquals(participant2, participant4);

        Participant participant5 = new Participant(1L, "nom", "prenom");
        participant5.setPrenom("prenom2");
        Assertions.assertNotEquals(participant2, participant5);

        Participant participant6 = new Participant(1L, "nom", "prenom");
        participant6.setCommentaire(null);
        Assertions.assertNotEquals(participant2, participant6);

        Participant participant7 = new Participant(1L, "nom", "prenom");
        participant7.setSondages(null);
        Assertions.assertNotEquals(participant2, participant7);

        Participant participant8 = new Participant(1L, "nom", "prenom");
        participant8.setDateSondee(null);
        Assertions.assertNotEquals(participant2, participant8);

        Assertions.assertNotEquals(null, participant);
        Assertions.assertNotEquals(participant, new Object());
    }

    @Test
    void hashCodeTest() {
        Assertions.assertEquals(participant.hashCode(), participant.hashCode());

        Participant participant3 = new Participant(1L, "nom", "prenom");
        participant3.setParticipantId(2L);
        Assertions.assertNotEquals(participant2.hashCode(), participant3.hashCode());

        Participant participant4 = new Participant(1L, "nom", "prenom");
        participant4.setNom("nom2");
        Assertions.assertNotEquals(participant2.hashCode(), participant4.hashCode());

        Participant participant5 = new Participant(1L, "nom", "prenom");
        participant5.setPrenom("prenom2");
        Assertions.assertNotEquals(participant2.hashCode(), participant5.hashCode());

        Participant participant6 = new Participant(1L, "nom", "prenom");
        participant6.setCommentaire(null);
        Assertions.assertNotEquals(participant2.hashCode(), participant6.hashCode());

        Participant participant7 = new Participant(1L, "nom", "prenom");
        participant7.setSondages(null);
        Assertions.assertNotEquals(participant2.hashCode(), participant7.hashCode());

        Participant participant8 = new Participant(1L, "nom", "prenom");
        participant8.setDateSondee(null);
        Assertions.assertNotEquals(participant2.hashCode(), participant8.hashCode());
    }

    @Test
    void toStringTest() {
        Assertions.assertEquals("Participant{participantId=1, nom='nom', prenom='prenom'}", participant2.toString());
    }

    @Test
    void testFieldsAnnotations() throws NoSuchFieldException {
        Field classMemberField = Participant.class.getDeclaredField("participantId");
        Assertions.assertTrue(classMemberField.isAnnotationPresent(jakarta.persistence.GeneratedValue.class));
        Assertions.assertTrue(classMemberField.isAnnotationPresent(jakarta.persistence.Id.class));

        Field classMemberField2 = Participant.class.getDeclaredField("commentaire");
        Assertions.assertTrue(classMemberField2.isAnnotationPresent(jakarta.persistence.OneToMany.class));
        Annotation[] annotations = classMemberField2.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof jakarta.persistence.OneToMany) {
                jakarta.persistence.OneToMany oneToMany = (jakarta.persistence.OneToMany) annotation;
                Assertions.assertEquals(jakarta.persistence.CascadeType.ALL, oneToMany.cascade()[0]);
                Assertions.assertEquals("participant", oneToMany.mappedBy());
            }
        }

        Field classMemberField3 = Participant.class.getDeclaredField("dateSondee");
        Assertions.assertTrue(classMemberField3.isAnnotationPresent(jakarta.persistence.OneToMany.class));
        Annotation[] annotations2 = classMemberField3.getAnnotations();
        for (Annotation annotation : annotations2) {
            if (annotation instanceof jakarta.persistence.OneToMany) {
                jakarta.persistence.OneToMany oneToMany = (jakarta.persistence.OneToMany) annotation;
                Assertions.assertEquals(jakarta.persistence.CascadeType.ALL, oneToMany.cascade()[0]);
                Assertions.assertEquals("participant", oneToMany.mappedBy());
            }
        }

        Field classMemberField4 = Participant.class.getDeclaredField("sondages");
        Assertions.assertTrue(classMemberField4.isAnnotationPresent(jakarta.persistence.OneToMany.class));
        Annotation[] annotations3 = classMemberField4.getAnnotations();
        for (Annotation annotation : annotations3) {
            if (annotation instanceof jakarta.persistence.OneToMany) {
                jakarta.persistence.OneToMany oneToMany = (jakarta.persistence.OneToMany) annotation;
                Assertions.assertEquals(jakarta.persistence.CascadeType.ALL, oneToMany.cascade()[0]);
                Assertions.assertEquals("createBy", oneToMany.mappedBy());
            }
        }
    }
}
