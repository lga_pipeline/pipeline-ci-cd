package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Date;

class DateSondageTest {

    private DateSondage dateSondage;
    private DateSondage dateSondage2;

    private Date date;

    @BeforeEach
    void setUp() {
        date = new Date();
        dateSondage = new DateSondage();
        dateSondage2 = new DateSondage(1L, date, null, null);
    }

    @Test
    void constructorTest() {
        //Test empty constructor
        Assertions.assertNull(dateSondage.getDateSondageId());
        Assertions.assertNull(dateSondage.getDate());
        Assertions.assertNotNull(dateSondage.getSondage());
        Assertions.assertNotNull(dateSondage.getDateSondee());

        //Test constructor with parameters
        Assertions.assertEquals(1L, dateSondage2.getDateSondageId());
        Assertions.assertEquals(date, dateSondage2.getDate());
        Assertions.assertNull(dateSondage2.getSondage());
        Assertions.assertNull(dateSondage2.getDateSondee());
    }

    @Test
    void gettersAndSettersTest() {
        dateSondage.setDateSondageId(1L);
        dateSondage.setDate(date);
        dateSondage.setSondage(null);
        dateSondage.setDateSondee(null);

        Assertions.assertEquals(1L, dateSondage.getDateSondageId());
        Assertions.assertEquals(date, dateSondage.getDate());
        Assertions.assertNull(dateSondage.getSondage());
        Assertions.assertNull(dateSondage.getDateSondee());
    }

    @Test
    void equalsTest() {
        Assertions.assertEquals(dateSondage, dateSondage);

        DateSondage dateSondage2 = new DateSondage();
        dateSondage2.setDateSondageId(2L);
        Assertions.assertNotEquals(dateSondage, dateSondage2);

        DateSondage dateSondage3 = new DateSondage();
        dateSondage3.setDate(new Date());
        Assertions.assertNotEquals(dateSondage, dateSondage3);

        DateSondage dateSondage4 = new DateSondage();
        dateSondage4.setSondage(null);
        Assertions.assertNotEquals(dateSondage, dateSondage4);

        DateSondage dateSondage5 = new DateSondage();
        dateSondage5.setDateSondee(null);
        Assertions.assertNotEquals(dateSondage, dateSondage5);

        Assertions.assertNotEquals(null, dateSondage);
        Assertions.assertNotEquals(dateSondage, new Object());
    }

    @Test
    void hashCodeTest() {
        Assertions.assertEquals(dateSondage.hashCode(), dateSondage.hashCode());

        DateSondage dateSondage2 = new DateSondage();
        dateSondage2.setDateSondageId(2L);
        Assertions.assertNotEquals(dateSondage.hashCode(), dateSondage2.hashCode());

        DateSondage dateSondage3 = new DateSondage();
        dateSondage3.setDate(new Date());
        Assertions.assertNotEquals(dateSondage.hashCode(), dateSondage3.hashCode());

        DateSondage dateSondage4 = new DateSondage();
        dateSondage4.setSondage(null);
        Assertions.assertNotEquals(dateSondage.hashCode(), dateSondage4.hashCode());

        DateSondage dateSondage5 = new DateSondage();
        dateSondage5.setDateSondee(null);
        Assertions.assertNotEquals(dateSondage.hashCode(), dateSondage5.hashCode());
    }

    @Test
    void testFieldsAnnotations() throws NoSuchFieldException {
        Field classMemberField = DateSondage.class.getDeclaredField("dateSondageId");
        Assertions.assertTrue(classMemberField.isAnnotationPresent(jakarta.persistence.GeneratedValue.class));
        Assertions.assertTrue(classMemberField.isAnnotationPresent(jakarta.persistence.Id.class));

        Field classMemberField2 = DateSondage.class.getDeclaredField("sondage");
        Assertions.assertTrue(classMemberField2.isAnnotationPresent(jakarta.persistence.ManyToOne.class));
        Annotation[] annotations = classMemberField2.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof jakarta.persistence.ManyToOne) {
                jakarta.persistence.ManyToOne manyToOne = (jakarta.persistence.ManyToOne) annotation;
                Assertions.assertEquals(jakarta.persistence.FetchType.LAZY, manyToOne.fetch());
            }
        }

        Field classMemberField3 = Participant.class.getDeclaredField("dateSondee");
        Assertions.assertTrue(classMemberField3.isAnnotationPresent(jakarta.persistence.OneToMany.class));
        Annotation[] annotations2 = classMemberField3.getAnnotations();
        for (Annotation annotation : annotations2) {
            if (annotation instanceof jakarta.persistence.OneToMany) {
                jakarta.persistence.OneToMany oneToMany = (jakarta.persistence.OneToMany) annotation;
                Assertions.assertEquals(jakarta.persistence.CascadeType.ALL, oneToMany.cascade()[0]);
                Assertions.assertEquals("participant", oneToMany.mappedBy());
            }
        }
    }
}
