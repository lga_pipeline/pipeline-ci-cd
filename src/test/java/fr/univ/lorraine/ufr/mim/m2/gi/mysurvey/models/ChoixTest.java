package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ChoixTest {

    @Test
    void testEnumSize() {
        assertEquals(3, Choix.values().length);
    }

    @Test
    void testDisponibleConstant() {
        assertEquals(0, Choix.DISPONIBLE.ordinal());
    }

    @Test
    void testIndisponibleConstant() {
        assertEquals(1, Choix.INDISPONIBLE.ordinal());
    }

    @Test
    void testPeutetreConstant() {
        assertEquals(2, Choix.PEUTETRE.ordinal());
    }

    @Test
    void testEnumValues() {
        Choix[] choix = Choix.values();
        Choix[] expected = {Choix.DISPONIBLE, Choix.INDISPONIBLE, Choix.PEUTETRE};
        assertArrayEquals(choix, expected);
    }
}
