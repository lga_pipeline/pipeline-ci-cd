package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Choix;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DateSondeeDtoTest {

    private DateSondeeDto dateSondeeDto;

    @BeforeEach
    void setUp() {
        dateSondeeDto = new DateSondeeDto();
    }

    @Test
    void constructorTest() {
        Assertions.assertNull(dateSondeeDto.getDateSondeeId());
        Assertions.assertNull(dateSondeeDto.getParticipant());
        Assertions.assertNull(dateSondeeDto.getChoix());
    }

    @Test
    void gettersAndSettersTest() {
        dateSondeeDto.setDateSondeeId(1L);
        dateSondeeDto.setParticipant(1L);
        dateSondeeDto.setChoix(Choix.DISPONIBLE.name());
        Assertions.assertEquals(1L, dateSondeeDto.getDateSondeeId());
        Assertions.assertEquals(1L, dateSondeeDto.getParticipant());
        Assertions.assertEquals(Choix.DISPONIBLE.name(), dateSondeeDto.getChoix());
    }

    @Test
    void testEquals() {
        Assertions.assertEquals(dateSondeeDto, dateSondeeDto);

        DateSondeeDto dateSondeeDto2 = new DateSondeeDto();
        dateSondeeDto2.setDateSondeeId(1L);
        Assertions.assertNotEquals(dateSondeeDto, dateSondeeDto2);

        DateSondeeDto dateSondeeDto3 = new DateSondeeDto();
        dateSondeeDto3.setParticipant(1L);
        Assertions.assertNotEquals(dateSondeeDto, dateSondeeDto3);

        DateSondeeDto dateSondeeDto4 = new DateSondeeDto();
        dateSondeeDto4.setChoix(Choix.PEUTETRE.name());
        Assertions.assertNotEquals(dateSondeeDto, dateSondeeDto4);

        Assertions.assertNotEquals(null, dateSondeeDto);
        Assertions.assertNotEquals(dateSondeeDto, new Object());
    }

    @Test
    void testHashCode() {
        Assertions.assertEquals(dateSondeeDto.hashCode(), dateSondeeDto.hashCode());

        DateSondeeDto dateSondeeDto2 = new DateSondeeDto();
        dateSondeeDto2.setDateSondeeId(1L);
        Assertions.assertNotEquals(dateSondeeDto.hashCode(), dateSondeeDto2.hashCode());
    }
}
