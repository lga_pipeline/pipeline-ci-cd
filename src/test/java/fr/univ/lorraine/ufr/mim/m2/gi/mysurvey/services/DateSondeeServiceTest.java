package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.*;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.DateSondageRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.ParticipantRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.SondageRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.util.ArrayList;
import java.util.Date;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class DateSondeeServiceTest {

    @Autowired
    DateSondeeService dateSondeeService;

    @Autowired
    SondageRepository sondageRepository;

    @Autowired
    ParticipantRepository participantRepository;

    @Autowired
    DateSondageRepository dateSondageRepository;

    @Test
    void whenCreate_withValidParameters() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondage.setCloture(false);
        sondageRepository.save(sondage);

        DateSondage dateSondage = dateSondageRepository.save(new DateSondage(1L, new Date(), sondage, new ArrayList<>()));

        DateSondee dateSondee = new DateSondee(1L, dateSondage, participant, Choix.DISPONIBLE);
        DateSondee createdDateSondee = dateSondeeService.create(dateSondage.getDateSondageId(), participant.getParticipantId(), dateSondee);

        Assertions.assertNotNull(createdDateSondee);
        Assertions.assertNotNull(createdDateSondee.getParticipant());
        Assertions.assertNotNull(createdDateSondee.getDateSondage());
    }

    @Test
    void whenCreate_withUnknownDateSondageId() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondage.setCloture(false);
        sondageRepository.save(sondage);

        DateSondage dateSondage = dateSondageRepository.save(new DateSondage(1L, new Date(), sondage, new ArrayList<>()));

        DateSondee dateSondee = new DateSondee(1L, dateSondage, participant, Choix.DISPONIBLE);
        DateSondee createdDateSondee = dateSondeeService.create(999L, participant.getParticipantId(), dateSondee);

        Assertions.assertNull(createdDateSondee);
    }

    @Test
    void whenCreate_withUnknownParticipantId() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondage.setCloture(false);
        sondageRepository.save(sondage);

        DateSondage dateSondage = dateSondageRepository.save(new DateSondage(1L, new Date(), sondage, new ArrayList<>()));

        DateSondee dateSondee = new DateSondee(1L, dateSondage, participant, Choix.DISPONIBLE);
        DateSondee createdDateSondee = dateSondeeService.create(dateSondage.getDateSondageId(), 999L, dateSondee);

        Assertions.assertNull(createdDateSondee);
    }

    @Test
    void whenCreate_withNullDateSondee() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondage.setCloture(false);
        sondageRepository.save(sondage);

        DateSondage dateSondage = dateSondageRepository.save(new DateSondage(1L, new Date(), sondage, new ArrayList<>()));

        DateSondee createdDateSondee = dateSondeeService.create(dateSondage.getDateSondageId(), participant.getParticipantId(), null);

        Assertions.assertNull(createdDateSondee);
    }

    @Test
    void whenCreate_withCloturedSondage() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondage.setCloture(true);
        sondageRepository.save(sondage);

        DateSondage dateSondage = dateSondageRepository.save(new DateSondage(1L, new Date(), sondage, new ArrayList<>()));

        DateSondee dateSondee = new DateSondee(1L, dateSondage, participant, Choix.DISPONIBLE);
        DateSondee createdDateSondee = dateSondeeService.create(dateSondage.getDateSondageId(), participant.getParticipantId(), dateSondee);

        Assertions.assertNull(createdDateSondee);
    }

    @Test
    void whenBestDate() {
        Participant participant1 = participantRepository.save(new Participant(1L, "test", "test"));
        Participant participant2 = participantRepository.save(new Participant(2L, "test", "test"));

        Sondage sondage1 = new Sondage();
        sondage1.setCreateBy(participant1);
        sondage1.setCloture(false);
        sondageRepository.save(sondage1);

        Sondage sondage2 = new Sondage();
        sondage2.setCreateBy(participant2);
        sondage2.setCloture(false);
        sondageRepository.save(sondage2);

        DateSondage dateSondage1 = dateSondageRepository.save(new DateSondage(1L, new Date(1675507187), sondage1, new ArrayList<>()));
        DateSondage dateSondage2 = dateSondageRepository.save(new DateSondage(2L, new Date(1675507167), sondage2, new ArrayList<>()));
        DateSondage dateSondage3 = dateSondageRepository.save(new DateSondage(3L, new Date(1675507147), sondage2, new ArrayList<>()));

        DateSondee dateSondee11 = dateSondeeService.create(dateSondage1.getDateSondageId(), participant1.getParticipantId(), new DateSondee(1L, dateSondage1, participant1, Choix.PEUTETRE));
        DateSondee dateSondee21 = dateSondeeService.create(dateSondage2.getDateSondageId(), participant1.getParticipantId(), new DateSondee(2L, dateSondage2, participant1, Choix.DISPONIBLE));
        DateSondee dateSondee31 = dateSondeeService.create(dateSondage3.getDateSondageId(), participant1.getParticipantId(), new DateSondee(3L, dateSondage3, participant1, Choix.DISPONIBLE));

        Assertions.assertEquals(0, dateSondeeService.bestDate(0L).size());
        Assertions.assertEquals(0, dateSondeeService.bestDate(1L).size());
        Assertions.assertEquals(2, dateSondeeService.bestDate(2L).size());

        DateSondee dateSondee32 = dateSondeeService.create(dateSondage3.getDateSondageId(), participant2.getParticipantId(), new DateSondee(4L, dateSondage3, participant2, Choix.DISPONIBLE));

        Assertions.assertEquals(1, dateSondeeService.bestDate(2L).size());
    }

    @Test
    void whenMaybyBestDate() {
        Participant participant1 = participantRepository.save(new Participant(1L, "test", "test"));
        Participant participant2 = participantRepository.save(new Participant(2L, "test", "test"));

        Sondage sondage1 = new Sondage();
        sondage1.setCreateBy(participant1);
        sondage1.setCloture(false);
        sondageRepository.save(sondage1);

        Sondage sondage2 = new Sondage();
        sondage2.setCreateBy(participant2);
        sondage2.setCloture(false);
        sondageRepository.save(sondage2);

        DateSondage dateSondage1 = dateSondageRepository.save(new DateSondage(1L, new Date(1675507187), sondage1, new ArrayList<>()));
        DateSondage dateSondage2 = dateSondageRepository.save(new DateSondage(2L, new Date(1675507167), sondage2, new ArrayList<>()));
        DateSondage dateSondage3 = dateSondageRepository.save(new DateSondage(3L, new Date(1675507147), sondage2, new ArrayList<>()));

        DateSondee dateSondee11 = dateSondeeService.create(dateSondage1.getDateSondageId(), participant1.getParticipantId(), new DateSondee(1L, dateSondage1, participant1, Choix.PEUTETRE));
        DateSondee dateSondee21 = dateSondeeService.create(dateSondage2.getDateSondageId(), participant1.getParticipantId(), new DateSondee(2L, dateSondage2, participant1, Choix.DISPONIBLE));
        DateSondee dateSondee31 = dateSondeeService.create(dateSondage3.getDateSondageId(), participant1.getParticipantId(), new DateSondee(3L, dateSondage3, participant1, Choix.PEUTETRE));

        Assertions.assertEquals(0, dateSondeeService.maybeBestDate(0L).size());
        Assertions.assertEquals(1, dateSondeeService.maybeBestDate(1L).size());
        Assertions.assertEquals(2, dateSondeeService.maybeBestDate(2L).size());

        DateSondee dateSondee32 = dateSondeeService.create(dateSondage3.getDateSondageId(), participant2.getParticipantId(), new DateSondee(4L, dateSondage3, participant2, Choix.PEUTETRE));

        Assertions.assertEquals(1, dateSondeeService.maybeBestDate(2L).size());
    }
}
