package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

class DateSondeeTest {

    private DateSondee dateSondee;
    private DateSondee dateSondee2;

    @BeforeEach
    void setUp() {
        dateSondee = new DateSondee();
        dateSondee2 = new DateSondee(1L, null, null, null);
    }

    @Test
    void constructorTest() {
        //Test empty constructor
        Assertions.assertNull(dateSondee.getDateSondeeId());
        Assertions.assertNotNull(dateSondee.getDateSondage());
        Assertions.assertNotNull(dateSondee.getParticipant());
        Assertions.assertNull(dateSondee.getChoix());

        //Test constructor with parameters
        Assertions.assertEquals(1L, dateSondee2.getDateSondeeId());
        Assertions.assertNull(dateSondee2.getDateSondage());
        Assertions.assertNull(dateSondee2.getParticipant());
        Assertions.assertNull(dateSondee2.getChoix());
    }

    @Test
    void gettersAndSettersTest() {
        dateSondee.setDateSondeeId(1L);
        dateSondee.setParticipant(null);
        dateSondee.setChoix(Choix.INDISPONIBLE.name());
        dateSondee.setDateSondage(null);

        Assertions.assertEquals(1L, dateSondee.getDateSondeeId());
        Assertions.assertEquals(Choix.INDISPONIBLE.name(), dateSondee.getChoix());
        Assertions.assertNull(dateSondee.getParticipant());
        Assertions.assertNull(dateSondee.getDateSondage());
    }

    @Test
    void equalsTest() {
        Assertions.assertEquals(dateSondee, dateSondee);

        DateSondee dateSondee2 = new DateSondee();
        dateSondee2.setDateSondeeId(2L);
        Assertions.assertNotEquals(dateSondee, dateSondee2);

        DateSondee dateSondee3 = new DateSondee();
        dateSondee3.setParticipant(null);
        Assertions.assertNotEquals(dateSondee, dateSondee3);

        DateSondee dateSondee4 = new DateSondee();
        dateSondee4.setDateSondage(null);
        Assertions.assertNotEquals(dateSondee, dateSondee4);

        DateSondee dateSondee5 = new DateSondee();
        dateSondee5.setChoix(Choix.DISPONIBLE.name());
        Assertions.assertNotEquals(dateSondee, dateSondee5);

        Assertions.assertNotEquals(null, dateSondee);
        Assertions.assertNotEquals(dateSondee, new Object());
    }

    @Test
    void hashCodeTest() {
        Assertions.assertEquals(dateSondee.hashCode(), dateSondee.hashCode());

        DateSondee dateSondee2 = new DateSondee();
        dateSondee2.setDateSondeeId(2L);
        Assertions.assertNotEquals(dateSondee.hashCode(), dateSondee2.hashCode());

        DateSondee dateSondee3 = new DateSondee();
        dateSondee3.setParticipant(null);
        Assertions.assertNotEquals(dateSondee.hashCode(), dateSondee3.hashCode());

        DateSondee dateSondee4 = new DateSondee();
        dateSondee4.setDateSondage(null);
        Assertions.assertNotEquals(dateSondee.hashCode(), dateSondee4.hashCode());

        DateSondee dateSondee5 = new DateSondee();
        dateSondee5.setChoix(Choix.DISPONIBLE.name());
        Assertions.assertNotEquals(dateSondee.hashCode(), dateSondee5.hashCode());
    }

    @Test
    void testFieldsAnnotations() throws NoSuchFieldException {
        Field classMemberField = DateSondee.class.getDeclaredField("dateSondeeId");
        Assertions.assertTrue(classMemberField.isAnnotationPresent(jakarta.persistence.GeneratedValue.class));
        Assertions.assertTrue(classMemberField.isAnnotationPresent(jakarta.persistence.Id.class));

        Field classMemberField2 = DateSondee.class.getDeclaredField("dateSondage");
        Assertions.assertTrue(classMemberField2.isAnnotationPresent(jakarta.persistence.ManyToOne.class));
        Annotation[] annotations = classMemberField2.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof jakarta.persistence.ManyToOne) {
                jakarta.persistence.ManyToOne manyToOne = (jakarta.persistence.ManyToOne) annotation;
                Assertions.assertEquals(jakarta.persistence.FetchType.LAZY, manyToOne.fetch());
            }
        }

        Field classMemberField3 = DateSondee.class.getDeclaredField("participant");
        Assertions.assertTrue(classMemberField3.isAnnotationPresent(jakarta.persistence.ManyToOne.class));
        Annotation[] annotations2 = classMemberField3.getAnnotations();
        for (Annotation annotation : annotations2) {
            if (annotation instanceof jakarta.persistence.ManyToOne) {
                jakarta.persistence.ManyToOne manyToOne = (jakarta.persistence.ManyToOne) annotation;
                Assertions.assertEquals(jakarta.persistence.FetchType.LAZY, manyToOne.fetch());
            }
        }
    }
}
