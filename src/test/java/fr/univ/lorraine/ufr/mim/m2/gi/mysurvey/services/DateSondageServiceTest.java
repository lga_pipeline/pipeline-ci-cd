package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.DateSondageRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.ParticipantRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.SondageRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class DateSondageServiceTest {

    @Autowired
    SondageRepository sondageRepository;

    @Autowired
    DateSondageService dateSondageService;

    @Autowired
    ParticipantRepository participantRepository;

    @Autowired
    DateSondageRepository dateSondageRepository;

    @Test
    void whenCreate_withValidSondageId() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondage.setCloture(false);
        sondageRepository.save(sondage);

        DateSondage dateSondage = dateSondageService.create(sondage.getSondageId(), new DateSondage());

        Assertions.assertNotNull(dateSondage);
    }

    @Test
    void whenCreate_withUnknownSondageId() {
        DateSondage dateSondage = dateSondageService.create(999L, new DateSondage());

        Assertions.assertNull(dateSondage);
    }

    @Test
    void whenCreate_withNullDateSondage() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondage.setCloture(false);
        sondageRepository.save(sondage);

        DateSondage dateSondage = dateSondageService.create(sondage.getSondageId(), null);

        Assertions.assertNull(dateSondage);
    }

    @Test
    void whenDelete_withValidDateSondageId() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondage.setCloture(false);
        sondageRepository.save(sondage);

        DateSondage dateSondage = dateSondageService.create(sondage.getSondageId(), new DateSondage());

        int result = dateSondageService.delete(dateSondage.getDateSondageId());

        Assertions.assertEquals(1, result);
        Assertions.assertNull(dateSondageService.getById(dateSondage.getDateSondageId()));
    }

    @Test
    void whenDelete_withUnknownDateSondageId() {
        int result = dateSondageService.delete(999L);

        Assertions.assertEquals(0, result);
    }

    @Test
    void whenGetById_withValidId() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondage.setCloture(false);
        sondageRepository.save(sondage);

        DateSondage dateSondage = dateSondageService.create(sondage.getSondageId(), new DateSondage());

        DateSondage result = dateSondageService.getById(dateSondage.getDateSondageId());

        Assertions.assertNotNull(result);
    }

    @Test
    void whenGetById_withUnknownId() {
        DateSondage result = dateSondageService.getById(999L);

        Assertions.assertNull(result);
    }

    @Test
    void whenGetBySondageId_withValidSondageId() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondage.setCloture(false);
        sondageRepository.save(sondage);

        dateSondageService.create(sondage.getSondageId(), new DateSondage());

        List<DateSondage> result = dateSondageService.getBySondageId(sondage.getSondageId());

        Assertions.assertNotNull(result);
    }

    @Test
    void whenGetBySondageId_withUnknownSondageId() {
        List<DateSondage> result = dateSondageService.getBySondageId(999L);

        Assertions.assertTrue(result.isEmpty());
    }

}

