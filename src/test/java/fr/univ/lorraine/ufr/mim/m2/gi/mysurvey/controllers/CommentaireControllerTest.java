package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.CommentaireDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Commentaire;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.CommentaireService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CommentaireController.class)
class CommentaireControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CommentaireService service;

    @MockBean
    ModelMapper mapper;

    ObjectMapper objectMapper = new ObjectMapper();

    private Commentaire commentaire;
    private CommentaireDto commentaireDto;
    private String commentaireDtoJson;

    @BeforeEach
    void setUp() throws JsonProcessingException {
        commentaire = new Commentaire();
        commentaireDto = new CommentaireDto();
        commentaireDtoJson = objectMapper.writeValueAsString(commentaireDto);
    }

    @Test
    void whenUpdate_withValidParameters_thenOK() throws Exception {
        when(mapper.map(commentaireDto, Commentaire.class)).thenReturn(commentaire);
        when(service.update(1L, commentaire)).thenReturn(new Commentaire());
        when(mapper.map(any(Commentaire.class), CommentaireDto.class)).thenReturn(commentaireDto);

        mockMvc.perform(put("/api/commentaire/1")
                        .content(commentaireDtoJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void whenUpdate_withInvalidParameters_thenNotFound() throws Exception {
        when(mapper.map(commentaireDto, Commentaire.class)).thenReturn(commentaire);
        when(service.update(1L, commentaire)).thenReturn(null);

        mockMvc.perform(put("/api/commentaire/1")
                        .content(commentaireDtoJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenUpdate_withInternalError_thenInternalServerError() throws Exception {
        when(mapper.map(commentaireDto, Commentaire.class)).thenReturn(commentaire);
        when(service.update(1L, commentaire)).thenThrow(new RuntimeException());

        mockMvc.perform(put("/api/commentaire/1")
                        .content(commentaireDtoJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenUpdate_withoutRequestBody_thenBadRequest() throws Exception {
        mockMvc.perform(put("/api/commentaire/1"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenDelete_withValidParameters_thenOK() throws Exception {
        when(service.delete(anyLong())).thenReturn(1);

        mockMvc.perform(delete("/api/commentaire/1"))
                .andExpect(status().isOk());
    }

    @Test
    void whenDelete_withInvalidParameters_thenNotFound() throws Exception {
        when(service.delete(anyLong())).thenReturn(0);

        mockMvc.perform(delete("/api/commentaire/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenDelete_withInternalError_thenInternalServerError() throws Exception {
        when(service.delete(anyLong())).thenThrow(new RuntimeException());

        mockMvc.perform(delete("/api/commentaire/1"))
                .andExpect(status().isInternalServerError());
    }
}
