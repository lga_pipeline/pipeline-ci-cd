package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

class SondageDtoTest {

    private SondageDto sondageDto;

    @BeforeEach
    void setUp() {
        sondageDto = new SondageDto();
    }

    @Test
    void constructorTest() {
        Assertions.assertNull(sondageDto.getSondageId());
        Assertions.assertNull(sondageDto.getNom());
        Assertions.assertNull(sondageDto.getDescription());
        Assertions.assertNull(sondageDto.getFin());
        Assertions.assertNull(sondageDto.getCloture());
        Assertions.assertNull(sondageDto.getCreateBy());
    }

    @Test
    void gettersAndSettersTest() {
        sondageDto.setSondageId(1L);
        sondageDto.setNom("nom");
        sondageDto.setDescription("description");
        sondageDto.setFin(new Date());
        sondageDto.setCloture(true);
        sondageDto.setCreateBy(1L);
        Assertions.assertEquals(1L, sondageDto.getSondageId());
        Assertions.assertEquals("nom", sondageDto.getNom());
        Assertions.assertEquals("description", sondageDto.getDescription());
        Assertions.assertEquals(true, sondageDto.getCloture());
        Assertions.assertEquals(1L, sondageDto.getCreateBy());
    }

    @Test
    void equalsTest() {
        Assertions.assertEquals(sondageDto, sondageDto);

        SondageDto sondageDto2 = new SondageDto();
        sondageDto2.setSondageId(1L);
        Assertions.assertNotEquals(sondageDto, sondageDto2);

        SondageDto sondageDto3 = new SondageDto();
        sondageDto3.setNom("nom");
        Assertions.assertNotEquals(sondageDto, sondageDto3);

        SondageDto sondageDto4 = new SondageDto();
        sondageDto4.setDescription("description");
        Assertions.assertNotEquals(sondageDto, sondageDto4);

        SondageDto sondageDto5 = new SondageDto();
        sondageDto5.setFin(new Date());
        Assertions.assertNotEquals(sondageDto, sondageDto5);

        SondageDto sondageDto6 = new SondageDto();
        sondageDto6.setCloture(true);
        Assertions.assertNotEquals(sondageDto, sondageDto6);

        SondageDto sondageDto7 = new SondageDto();
        sondageDto7.setCreateBy(1L);
        Assertions.assertNotEquals(sondageDto, sondageDto7);

        Assertions.assertNotEquals(null, sondageDto);
        Assertions.assertNotEquals(sondageDto, new Object());
    }

    @Test
    void hashCodeTest() {
        Assertions.assertEquals(sondageDto.hashCode(), sondageDto.hashCode());

        SondageDto sondageDto2 = new SondageDto();
        sondageDto2.setSondageId(1L);
        Assertions.assertNotEquals(sondageDto.hashCode(), sondageDto2.hashCode());

        SondageDto sondageDto3 = new SondageDto();
        sondageDto3.setNom("nom");
        Assertions.assertNotEquals(sondageDto.hashCode(), sondageDto3.hashCode());

        SondageDto sondageDto4 = new SondageDto();
        sondageDto4.setDescription("description");
        Assertions.assertNotEquals(sondageDto.hashCode(), sondageDto4.hashCode());

        SondageDto sondageDto5 = new SondageDto();
        sondageDto5.setFin(new Date());
        Assertions.assertNotEquals(sondageDto.hashCode(), sondageDto5.hashCode());

        SondageDto sondageDto6 = new SondageDto();
        sondageDto6.setCloture(true);
        Assertions.assertNotEquals(sondageDto.hashCode(), sondageDto6.hashCode());

        SondageDto sondageDto7 = new SondageDto();
        sondageDto7.setCreateBy(1L);
        Assertions.assertNotEquals(sondageDto.hashCode(), sondageDto7.hashCode());
    }
}
