package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Commentaire;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.ParticipantRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.SondageRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class CommentaireServiceTest {

    @Autowired
    CommentaireService commentaireService;

    @Autowired
    SondageRepository sondageRepository;

    @Autowired
    ParticipantRepository participantRepository;

    @Test
    void whenAddCommentaire_withValidsParameters() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondageRepository.save(sondage);

        Commentaire createdCommentaire = commentaireService.addCommantaire(sondage.getSondageId(), participant.getParticipantId(), new Commentaire());

        Assertions.assertNotNull(createdCommentaire);
    }

    @Test
    void whenAddCommentaire_withUnknownSondageId() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Commentaire createdCommentaire = commentaireService.addCommantaire(999L, participant.getParticipantId(), new Commentaire());

        Assertions.assertNull(createdCommentaire);
    }

    @Test
    void whenAddCommentaire_withUnknownParticipantId() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondageRepository.save(sondage);

        Commentaire createdCommentaire = commentaireService.addCommantaire(sondage.getSondageId(), 999L, new Commentaire());

        Assertions.assertNull(createdCommentaire);
    }

    @Test
    void whenAddCommentaire_withNullCommentaire() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondageRepository.save(sondage);

        Commentaire createdCommentaire = commentaireService.addCommantaire(sondage.getSondageId(), participant.getParticipantId(), null);

        Assertions.assertNull(createdCommentaire);
    }

    @Test
    void whenUpdateCommentaire_withValidCommentaireId() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondageRepository.save(sondage);

        Commentaire createdCommentaire = commentaireService.addCommantaire(sondage.getSondageId(), participant.getParticipantId(), new Commentaire());
        Commentaire createdCommentaire2 = commentaireService.addCommantaire(sondage.getSondageId(), participant.getParticipantId(), new Commentaire());

        createdCommentaire2.setContenu("test");
        Commentaire updatedCommentaire = commentaireService.update(createdCommentaire.getCommentaireId(), createdCommentaire2);
        Assertions.assertNotNull(updatedCommentaire);
        Assertions.assertEquals(createdCommentaire2.getCommentaireId(), updatedCommentaire.getCommentaireId());
        Assertions.assertEquals("test", updatedCommentaire.getContenu());
    }

    @Test
    void whenUpdateCommentaire_withUnknownCommentaireId() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondageRepository.save(sondage);

        Commentaire createdCommentaire = commentaireService.addCommantaire(sondage.getSondageId(), participant.getParticipantId(), new Commentaire());

        createdCommentaire.setContenu("test");
        Commentaire updatedCommentaire = commentaireService.update(999L, createdCommentaire);

        Assertions.assertNull(updatedCommentaire);
    }

    @Test
    void whenUpdateCommentaire_withNullCommentaire() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondageRepository.save(sondage);

        Commentaire createdCommentaire = commentaireService.addCommantaire(sondage.getSondageId(), participant.getParticipantId(), new Commentaire());

        createdCommentaire.setContenu("test");
        Commentaire updatedCommentaire = commentaireService.update(createdCommentaire.getCommentaireId(), null);

        Assertions.assertNull(updatedCommentaire);
    }

    @Test
    void whenDeleteCommentaire_withValidCommentaireId() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondageRepository.save(sondage);

        Commentaire createdCommentaire = commentaireService.addCommantaire(sondage.getSondageId(), participant.getParticipantId(), new Commentaire());

        Assertions.assertEquals(1, commentaireService.delete(createdCommentaire.getCommentaireId()));
        Assertions.assertTrue(commentaireService.getBySondageId(createdCommentaire.getCommentaireId()).isEmpty());
    }

    @Test
    void whenDeleteCommentaire_withUnknownCommentaireId() {
        Assertions.assertEquals(0, commentaireService.delete(999L));
    }

    @Test
    void whenGetBySondageId_withValidSondageId() {
        Participant participant = participantRepository.save(new Participant(1L, "test", "test"));

        Sondage sondage = new Sondage();
        sondage.setCreateBy(participant);
        sondageRepository.save(sondage);

        commentaireService.addCommantaire(sondage.getSondageId(), participant.getParticipantId(), new Commentaire());

        Assertions.assertEquals(1, commentaireService.getBySondageId(sondage.getSondageId()).size());
    }

    @Test
    void whenGetBySondageId_withUnknownSondageId() {
        Assertions.assertEquals(0, commentaireService.getBySondageId(999L).size());
    }

}
