package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.ParticipantRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class SondageServiceTest {

    @Autowired
    ParticipantRepository participantRepository;

    @Autowired
    SondageService sondageService;

    @Test
    void whenCreate_withValidParticipantId() {
        Participant participant = new Participant(1L, "test", "test");
        participantRepository.save(participant);

        Sondage sondage = new Sondage();

        Sondage createdSondage = sondageService.create(1L, sondage);

        Assertions.assertNotNull(createdSondage);
        Assertions.assertEquals(1L, createdSondage.getCreateBy().getParticipantId());
    }

    @Test
    void whenCreate_withUnknownParticipantId() {
        Sondage sondage = new Sondage();

        Sondage createdSondage = sondageService.create(999L, sondage);

        Assertions.assertNull(createdSondage);
    }

    @Test
    void whenCreate_withNullSondage() {
        Sondage createdSondage = sondageService.create(1L, null);

        Assertions.assertNull(createdSondage);
    }

    @Test
    void whenUpdate_withValidSondageId() {
        Sondage sondage = new Sondage();
        sondage.setNom("test");
        sondage.setCreateBy(participantRepository.save(new Participant(1L, "test", "test")));
        Sondage createdSondage = sondageService.create(1L, sondage);

        createdSondage.setNom("test2");
        Sondage updatedSondage = sondageService.update(createdSondage.getSondageId(), sondage);

        Assertions.assertNotNull(updatedSondage);
        Assertions.assertEquals("test2", updatedSondage.getNom());
        Assertions.assertEquals(createdSondage.getSondageId(), updatedSondage.getSondageId());
    }

    @Test
    void whenUpdate_withUnknownSondageId() {
        Sondage sondage = new Sondage();
        sondage.setNom("test");
        sondage.setCreateBy(participantRepository.save(new Participant(1L, "test", "test")));
        Sondage createdSondage = sondageService.create(1L, sondage);

        createdSondage.setNom("test2");
        Sondage updatedSondage = sondageService.update(999L, sondage);
        Assertions.assertNull(updatedSondage);
    }

    @Test
    void whenUpdate_withNullSondage() {
        Sondage sondage = new Sondage();
        sondage.setNom("test");
        sondage.setCreateBy(participantRepository.save(new Participant(1L, "test", "test")));
        Sondage createdSondage = sondageService.create(1L, sondage);

        createdSondage.setNom("test2");
        Sondage updatedSondage = sondageService.update(createdSondage.getSondageId(), null);

        Assertions.assertNull(updatedSondage);
    }

    @Test
    void whenDelete_withValidId() {
        Sondage sondage = new Sondage();
        sondage.setNom("test");
        sondage.setCreateBy(participantRepository.save(new Participant(1L, "test", "test")));
        Sondage createdSondage = sondageService.create(1L, sondage);

        Assertions.assertEquals(1, sondageService.delete(createdSondage.getSondageId()));
        Assertions.assertNull(sondageService.getById(createdSondage.getSondageId()));
    }

    @Test
    void whenDelete_withUnknownId() {
        Assertions.assertEquals(0, sondageService.delete(999L));
    }

    @Test
    void whenGetById_withValidId() {
        Sondage sondage = new Sondage();
        sondage.setNom("test");
        sondage.setCreateBy(participantRepository.save(new Participant(1L, "test", "test")));
        Sondage createdSondage = sondageService.create(1L, sondage);

        Sondage foundSondage = sondageService.getById(createdSondage.getSondageId());

        Assertions.assertNotNull(foundSondage);
        Assertions.assertEquals(createdSondage.getSondageId(), foundSondage.getSondageId());
    }

    @Test
    void whenGetById_withUnknownId() {
        Sondage foundSondage = sondageService.getById(999L);

        Assertions.assertNull(foundSondage);
    }

    @Test
    void whenGetAll_withEmptyDatabase() {
        Assertions.assertEquals(0, sondageService.getAll().size());
    }

    @Test
    void whenGetAll_withNoEmptyDatabase() {
        participantRepository.save(new Participant(1L, "test", "test"));
        participantRepository.save(new Participant(2L, "test2", "test2"));
        sondageService.create(1L, new Sondage());
        sondageService.create(2L, new Sondage());

        Assertions.assertEquals(2, sondageService.getAll().size());
    }

}
