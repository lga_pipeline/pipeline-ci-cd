package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class ParticipantServiceTest {

    @Autowired
    ParticipantService participantService;

    @Test
    void whenCreate_withValidParticipant() {
        Participant participant = new Participant(1L, "test", "test");
        Participant createdParticipant = participantService.create(participant);

        Assertions.assertNotNull(createdParticipant);
        Assertions.assertEquals("test", createdParticipant.getNom());
    }

    @Test
    void whenCreate_withNullParticipant() {
        Participant createdParticipant = participantService.create(null);

        Assertions.assertNull(createdParticipant);
    }

    @Test
    void whenUpdate_withValidParticipantId() {
        Participant participant = new Participant(1L, "test", "test");
        Participant createdParticipant = participantService.create(participant);

        createdParticipant.setNom("test2");
        Participant updatedParticipant = participantService.update(createdParticipant.getParticipantId(), createdParticipant);

        Assertions.assertNotNull(updatedParticipant);
        Assertions.assertEquals("test2", updatedParticipant.getNom());
        Assertions.assertEquals(createdParticipant.getParticipantId(), updatedParticipant.getParticipantId());
    }

    @Test
    void whenUpdate_withUnknownParticipantId() {
        Participant participant = new Participant(1L, "test", "test");
        Participant createdParticipant = participantService.create(participant);

        createdParticipant.setNom("test2");
        Participant updatedParticipant = participantService.update(999L, createdParticipant);

        Assertions.assertNull(updatedParticipant);
    }

    @Test
    void whenUpdate_withNullParticipant() {
        Participant updatedParticipant = participantService.update(1L, null);

        Assertions.assertNull(updatedParticipant);
    }

    @Test
    void whenDelete_withValidParticipantId() {
        Participant participant = new Participant(1L, "test", "test");
        Participant createdParticipant = participantService.create(participant);

        Assertions.assertEquals(1, participantService.delete(createdParticipant.getParticipantId()));
        Assertions.assertNull(participantService.getById(createdParticipant.getParticipantId()));
    }

    @Test
    void whenDelete_withUnknownParticipantId() {
        Assertions.assertEquals(0, participantService.delete(999L));
    }

    @Test
    void whenGetById_withValidParticipantId() {
        Participant participant = new Participant(1L, "test", "test");
        Participant createdParticipant = participantService.create(participant);

        Assertions.assertNotNull(participantService.getById(createdParticipant.getParticipantId()));
    }

    @Test
    void whenGetById_withUnknownParticipantId() {
        Assertions.assertNull(participantService.getById(999L));
    }

    @Test
    void whenGetAll_withEmptyDatabase() {
        Assertions.assertEquals(0, participantService.getAll().size());
    }

    @Test
    void whenGetAll_withNoEmptyDatabase() {
        participantService.create(new Participant(1L, "test", "test"));
        participantService.create(new Participant(2L, "test2", "test2"));

        Assertions.assertEquals(2, participantService.getAll().size());
    }

}
