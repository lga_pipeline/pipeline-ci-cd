package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CommentaireDtoTest {

    private CommentaireDto commentaireDto;

    @BeforeEach
    void setUp() {
        commentaireDto = new CommentaireDto();
    }

    @Test
    void constructorTest() {
        Assertions.assertNull(commentaireDto.getCommentaireId());
        Assertions.assertNull(commentaireDto.getCommentaire());
        Assertions.assertNull(commentaireDto.getParticipant());
    }

    @Test
    void gettersAndSettersTest() {
        commentaireDto.setCommentaireId(1L);
        commentaireDto.setCommentaire("commentaire");
        commentaireDto.setParticipant(1L);
        Assertions.assertEquals(1L, commentaireDto.getCommentaireId());
        Assertions.assertEquals("commentaire", commentaireDto.getCommentaire());
        Assertions.assertEquals(1L, commentaireDto.getParticipant());
    }

    @Test
    void testEquals() {
        Assertions.assertEquals(commentaireDto, commentaireDto);

        CommentaireDto commentaireDto2 = new CommentaireDto();
        commentaireDto2.setCommentaireId(1L);
        Assertions.assertNotEquals(commentaireDto, commentaireDto2);

        CommentaireDto commentaireDto3 = new CommentaireDto();
        commentaireDto3.setParticipant(1L);
        Assertions.assertNotEquals(commentaireDto, commentaireDto3);

        CommentaireDto commentaireDto4 = new CommentaireDto();
        commentaireDto4.setCommentaire("commentaire2");
        Assertions.assertNotEquals(commentaireDto, commentaireDto4);

        Assertions.assertNotEquals(null, commentaireDto);
        Assertions.assertNotEquals(commentaireDto, new Object());
    }

    @Test
    void testHashCode() {
        Assertions.assertEquals(commentaireDto.hashCode(), commentaireDto.hashCode());

        CommentaireDto commentaireDto2 = new CommentaireDto();
        commentaireDto2.setCommentaireId(1L);
        Assertions.assertNotEquals(commentaireDto.hashCode(), commentaireDto2.hashCode());

        CommentaireDto commentaireDto3 = new CommentaireDto();
        commentaireDto3.setParticipant(1L);
        Assertions.assertNotEquals(commentaireDto.hashCode(), commentaireDto3.hashCode());

        CommentaireDto commentaireDto4 = new CommentaireDto();
        commentaireDto4.setCommentaire("commentaire2");
        Assertions.assertNotEquals(commentaireDto.hashCode(), commentaireDto4.hashCode());
    }
}
