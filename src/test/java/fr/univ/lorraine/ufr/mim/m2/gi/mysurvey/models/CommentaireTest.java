package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

class CommentaireTest {

    private Commentaire commentaire;

    @BeforeEach
    void setUp() {
        commentaire = new Commentaire();
    }

    @Test
    void constructorTest() {
        //Test empty constructor
        Assertions.assertNull(commentaire.getCommentaireId());
        Assertions.assertNull(commentaire.getContenu());
        Assertions.assertNotNull(commentaire.getSondage());
        Assertions.assertNotNull(commentaire.getParticipant());
    }

    @Test
    void gettersAndSettersTest() {
        commentaire.setCommentaireId(1L);
        commentaire.setContenu("commentaire");
        commentaire.setSondage(null);
        commentaire.setParticipant(null);

        Assertions.assertEquals(1L, commentaire.getCommentaireId());
        Assertions.assertEquals("commentaire", commentaire.getContenu());
        Assertions.assertNull(commentaire.getSondage());
        Assertions.assertNull(commentaire.getParticipant());
    }

    @Test
    void equalsTest() {
        Assertions.assertEquals(commentaire, commentaire);

        Commentaire commentaire2 = new Commentaire();
        commentaire2.setCommentaireId(2L);
        Assertions.assertNotEquals(commentaire, commentaire2);

        Commentaire commentaire3 = new Commentaire();
        commentaire3.setContenu("commentaire2");
        Assertions.assertNotEquals(commentaire, commentaire3);

        Commentaire commentaire4 = new Commentaire();
        commentaire4.setSondage(null);
        Assertions.assertNotEquals(commentaire, commentaire4);

        Commentaire commentaire5 = new Commentaire();
        commentaire5.setParticipant(null);
        Assertions.assertNotEquals(commentaire, commentaire5);

        Assertions.assertNotEquals(null, commentaire);
        Assertions.assertNotEquals(commentaire, new Object());
    }

    @Test
    void hashCodeTest() {
        Assertions.assertEquals(commentaire.hashCode(), commentaire.hashCode());

        Commentaire commentaire2 = new Commentaire();
        commentaire2.setCommentaireId(2L);
        Assertions.assertNotEquals(commentaire.hashCode(), commentaire2.hashCode());

        Commentaire commentaire3 = new Commentaire();
        commentaire3.setContenu("commentaire2");
        Assertions.assertNotEquals(commentaire.hashCode(), commentaire3.hashCode());

        Commentaire commentaire4 = new Commentaire();
        commentaire4.setSondage(null);
        Assertions.assertNotEquals(commentaire.hashCode(), commentaire4.hashCode());

        Commentaire commentaire5 = new Commentaire();
        commentaire5.setParticipant(null);
        Assertions.assertNotEquals(commentaire.hashCode(), commentaire5.hashCode());
    }

    @Test
    void testFieldsAnnotations() throws NoSuchFieldException {
        Field classMemberField = Commentaire.class.getDeclaredField("commentaireId");
        Assertions.assertTrue(classMemberField.isAnnotationPresent(jakarta.persistence.GeneratedValue.class));
        Assertions.assertTrue(classMemberField.isAnnotationPresent(jakarta.persistence.Id.class));

        Field classMemberField2 = Commentaire.class.getDeclaredField("sondage");
        Assertions.assertTrue(classMemberField2.isAnnotationPresent(jakarta.persistence.ManyToOne.class));
        Annotation[] annotations = classMemberField2.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof jakarta.persistence.ManyToOne) {
                jakarta.persistence.ManyToOne manyToOne = (jakarta.persistence.ManyToOne) annotation;
                Assertions.assertEquals(jakarta.persistence.FetchType.LAZY, manyToOne.fetch());
            }
        }

        Field classMemberField3 = Commentaire.class.getDeclaredField("participant");
        Assertions.assertTrue(classMemberField3.isAnnotationPresent(jakarta.persistence.ManyToOne.class));
        Annotation[] annotations2 = classMemberField3.getAnnotations();
        for (Annotation annotation : annotations2) {
            if (annotation instanceof jakarta.persistence.ManyToOne) {
                jakarta.persistence.ManyToOne manyToOne = (jakarta.persistence.ManyToOne) annotation;
                Assertions.assertEquals(jakarta.persistence.FetchType.LAZY, manyToOne.fetch());
            }
        }
    }
}
