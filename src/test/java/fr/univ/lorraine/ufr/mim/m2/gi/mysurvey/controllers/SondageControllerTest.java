package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.CommentaireDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.DateSondageDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.SondageDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Commentaire;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.CommentaireService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondageService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondeeService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.SondageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(SondageController.class)
class SondageControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    SondageService sondageService;

    @MockBean
    CommentaireService commentaireService;

    @MockBean
    DateSondageService dateSondageService;

    @MockBean
    DateSondeeService dateSondeeService;

    @MockBean
    ModelMapper mapper;

    ObjectMapper objectMapper = new ObjectMapper();

    private Sondage sondage;
    private SondageDto sondageDto;
    private String sondageDtoJson;

    private DateSondage dateSondage;
    private DateSondageDto dateSondageDto;

    private Commentaire commentaire;
    private CommentaireDto commentaireDto;
    private String commentaireDtoJson;

    @BeforeEach
    void setUp() throws JsonProcessingException {
        sondage = new Sondage();
        sondageDto = new SondageDto();
        sondageDto.setCreateBy(1L);
        sondageDtoJson = objectMapper.writeValueAsString(sondageDto);

        dateSondage = new DateSondage();
        dateSondageDto = new DateSondageDto();

        commentaire = new Commentaire();
        commentaireDto = new CommentaireDto();
        commentaireDto.setParticipant(1L);
        commentaireDtoJson = objectMapper.writeValueAsString(commentaireDto);
    }

    @Test
    void whenDelete_withValidParameter_thenOK() throws Exception {
        when(sondageService.delete(anyLong())).thenReturn(1);

        mockMvc.perform(delete("/api/sondage/1"))
                .andExpect(status().isOk());
    }

    @Test
    void whenDelete_withInvalidParameter_thenNotFound() throws Exception {
        when(sondageService.delete(anyLong())).thenReturn(0);

        mockMvc.perform(delete("/api/sondage/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenDelete_withInternalError_thenInternalServerError() throws Exception {
        when(sondageService.delete(anyLong())).thenThrow(new RuntimeException());

        mockMvc.perform(delete("/api/sondage/1"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenUpdate_withValidParameters_thenOK() throws Exception {
        when(mapper.map(sondageDto, Sondage.class)).thenReturn(sondage);
        when(sondageService.update(1L, sondage)).thenReturn(sondage);
        when(mapper.map(sondage, SondageDto.class)).thenReturn(sondageDto);

        mockMvc.perform(put("/api/sondage/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(sondageDtoJson))
                .andExpect(status().isOk());
    }

    @Test
    void whenUpdate_withInvalidParameters_thenNotFound() throws Exception {
        when(mapper.map(sondageDto, Sondage.class)).thenReturn(sondage);
        when(sondageService.update(1L, sondage)).thenReturn(null);

        mockMvc.perform(put("/api/sondage/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(sondageDtoJson))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenUpdate_withInternalError_thenInternalServerError() throws Exception {
        when(mapper.map(sondageDto, Sondage.class)).thenReturn(sondage);
        when(sondageService.update(1L, sondage)).thenThrow(new RuntimeException());

        mockMvc.perform(put("/api/sondage/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(sondageDtoJson))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenUpdate_withoutRequestBody_thenBadRequest() throws Exception {
        mockMvc.perform(put("/api/sondage/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenCreateDate_withValidParameters_thenCreated() throws Exception {
        when(mapper.map(dateSondageDto, DateSondage.class)).thenReturn(dateSondage);
        when(dateSondageService.create(1L, dateSondage)).thenReturn(dateSondage);
        when(mapper.map(dateSondage, DateSondageDto.class)).thenReturn(dateSondageDto);

        mockMvc.perform(post("/api/sondage/1/dates")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(sondageDtoJson))
                .andExpect(status().isCreated());
    }

    @Test
    void whenCreateDate_withInvalidParameters_thenNotFound() throws Exception {
        when(mapper.map(dateSondageDto, DateSondage.class)).thenReturn(dateSondage);
        when(dateSondageService.create(1L, dateSondage)).thenReturn(null);

        mockMvc.perform(post("/api/sondage/1/dates")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(sondageDtoJson))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenCreateDate_withInternalError_thenInternalServerError() throws Exception {
        when(mapper.map(dateSondageDto, DateSondage.class)).thenReturn(dateSondage);
        when(dateSondageService.create(1L, dateSondage)).thenThrow(new RuntimeException());

        mockMvc.perform(post("/api/sondage/1/dates")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(sondageDtoJson))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenCreateDate_withoutRequestBody_thenBadRequest() throws Exception {
        mockMvc.perform(post("/api/sondage/1/dates"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenCreateCommentaire_withValidParameters_thenCreated() throws Exception {
        when(mapper.map(commentaireDto, Commentaire.class)).thenReturn(commentaire);
        when(commentaireService.addCommantaire(1L, 1L, commentaire)).thenReturn(commentaire);
        when(mapper.map(commentaire, CommentaireDto.class)).thenReturn(commentaireDto);

        mockMvc.perform(post("/api/sondage/1/commentaires")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(commentaireDtoJson))
                .andExpect(status().isCreated());
    }

    @Test
    void whenCreateCommentaire_withInvalidParameters_thenNotFound() throws Exception {
        when(mapper.map(commentaireDto, Commentaire.class)).thenReturn(commentaire);
        when(commentaireService.addCommantaire(1L, 1L, commentaire)).thenReturn(null);

        mockMvc.perform(post("/api/sondage/1/commentaires")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(commentaireDtoJson))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenCreateCommentaire_withInternalError_thenInternalServerError() throws Exception {
        when(mapper.map(commentaireDto, Commentaire.class)).thenReturn(commentaire);
        when(commentaireService.addCommantaire(1L, 1L, commentaire)).thenThrow(new RuntimeException());

        mockMvc.perform(post("/api/sondage/1/commentaires")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(commentaireDtoJson))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenCreateCommentaire_withoutRequestBody_thenBadRequest() throws Exception {
        mockMvc.perform(post("/api/sondage/1/commentaires"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenCreate_withValidParameters_thenCreated() throws Exception {
        when(mapper.map(sondageDto, Sondage.class)).thenReturn(sondage);
        when(sondageService.create(1L, sondage)).thenReturn(sondage);
        when(mapper.map(sondage, SondageDto.class)).thenReturn(sondageDto);

        mockMvc.perform(post("/api/sondage/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(sondageDtoJson))
                .andExpect(status().isCreated());
    }

    @Test
    void whenCreate_withInvalidParameters_thenNotFound() throws Exception {
        when(mapper.map(sondageDto, Sondage.class)).thenReturn(sondage);
        when(sondageService.create(1L, sondage)).thenReturn(null);

        mockMvc.perform(post("/api/sondage/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(sondageDtoJson))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenCreate_withoutRequestBody_thenBadRequest() throws Exception {
        mockMvc.perform(post("/api/sondage/"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenCreate_withInternalError_thenInternalServerError() throws Exception {
        when(mapper.map(sondageDto, Sondage.class)).thenReturn(sondage);
        when(sondageService.create(1L, sondage)).thenThrow(new RuntimeException());

        mockMvc.perform(post("/api/sondage/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(sondageDtoJson))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenGetDates_withValidParameters_thenOK() throws Exception {
        when(dateSondageService.getBySondageId(1L)).thenReturn(List.of(dateSondage));
        when(mapper.map(dateSondage, DateSondageDto.class)).thenReturn(dateSondageDto);

        mockMvc.perform(get("/api/sondage/1/dates"))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetDates_withInvalidParameters_thenNoContent() throws Exception {
        when(dateSondageService.getBySondageId(1L)).thenReturn(List.of());

        mockMvc.perform(get("/api/sondage/1/dates"))
                .andExpect(status().isNoContent());
    }

    @Test
    void whenGetDates_withInternalError_thenInternalServerError() throws Exception {
        when(dateSondageService.getBySondageId(1L)).thenThrow(new RuntimeException());

        mockMvc.perform(get("/api/sondage/1/dates"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenGetCommentaires_withValidParameters_thenOK() throws Exception {
        when(commentaireService.getBySondageId(1L)).thenReturn(List.of(commentaire));
        when(mapper.map(commentaire, CommentaireDto.class)).thenReturn(commentaireDto);

        mockMvc.perform(get("/api/sondage/1/commentaires"))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetCommentaires_withInvalidParameters_thenNoContent() throws Exception {
        when(commentaireService.getBySondageId(1L)).thenReturn(List.of());

        mockMvc.perform(get("/api/sondage/1/commentaires"))
                .andExpect(status().isNoContent());
    }

    @Test
    void whenGetCommentaires_withInternalError_thenInternalServerError() throws Exception {
        when(commentaireService.getBySondageId(1L)).thenThrow(new RuntimeException());

        mockMvc.perform(get("/api/sondage/1/commentaires"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenGetAll_withContent_thenOK() throws Exception {
        when(sondageService.getAll()).thenReturn(List.of(sondage));
        when(mapper.map(sondage, SondageDto.class)).thenReturn(sondageDto);

        mockMvc.perform(get("/api/sondage/"))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetAll_withNoContent_thenNoContent() throws Exception {
        when(sondageService.getAll()).thenReturn(List.of());

        mockMvc.perform(get("/api/sondage/"))
                .andExpect(status().isNoContent());
    }

    @Test
    void whenGetAll_withInternalError_thenInternalServerError() throws Exception {
        when(sondageService.getAll()).thenThrow(new RuntimeException());

        mockMvc.perform(get("/api/sondage/"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenGetMaybeBest_withValidParameters_thenOK() throws Exception {
        when(dateSondeeService.maybeBestDate(1L)).thenReturn(List.of(new Date()));

        mockMvc.perform(get("/api/sondage/1/maybe"))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetMaybeBest_withInvalidParameters_thenNoContent() throws Exception {
        when(dateSondeeService.maybeBestDate(1L)).thenReturn(List.of());

        mockMvc.perform(get("/api/sondage/1/maybe"))
                .andExpect(status().isNoContent());
    }

    @Test
    void whenGetMaybeBest_withInternalError_thenInternalServerError() throws Exception {
        when(dateSondeeService.maybeBestDate(1L)).thenThrow(new RuntimeException());

        mockMvc.perform(get("/api/sondage/1/maybe"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenGetBest_withValidParameters_thenOK() throws Exception {
        when(dateSondeeService.bestDate(1L)).thenReturn(List.of(new Date()));

        mockMvc.perform(get("/api/sondage/1/best"))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetBest_withInvalidParameters_thenNoContent() throws Exception {
        when(dateSondeeService.bestDate(1L)).thenReturn(List.of());

        mockMvc.perform(get("/api/sondage/1/best"))
                .andExpect(status().isNoContent());
    }

    @Test
    void whenGetBest_withInternalError_thenInternalServerError() throws Exception {
        when(dateSondeeService.bestDate(1L)).thenThrow(new RuntimeException());

        mockMvc.perform(get("/api/sondage/1/best"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenGet_withValidParameters_thenOK() throws Exception {
        when(sondageService.getById(1L)).thenReturn(sondage);
        when(mapper.map(sondage, SondageDto.class)).thenReturn(sondageDto);

        mockMvc.perform(get("/api/sondage/1"))
                .andExpect(status().isOk());
    }

    @Test
    void whenGet_withInvalidParameters_thenNotFound() throws Exception {
        when(sondageService.getById(1L)).thenReturn(null);

        mockMvc.perform(get("/api/sondage/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenGet_withInternalError_thenInternalServerError() throws Exception {
        when(sondageService.getById(1L)).thenThrow(new RuntimeException());

        mockMvc.perform(get("/api/sondage/1"))
                .andExpect(status().isInternalServerError());
    }

}
