package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.ParticipantDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.ParticipantService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ParticipantController.class)
class ParticipantControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ParticipantService participantService;

    @MockBean
    ModelMapper modelMapper;

    ObjectMapper objectMapper = new ObjectMapper();

    private Participant participant;
    private ParticipantDto participantDto;
    private String participantDtoJson;

    @BeforeEach
    void setUp() throws JsonProcessingException {
        participant = new Participant();
        participantDto = new ParticipantDto();
        participantDtoJson = objectMapper.writeValueAsString(participantDto);
    }

    @Test
    void whenCreateParticipant_withValidParameters_thenCreated() throws Exception {
        when(modelMapper.map(participantDto, Participant.class)).thenReturn(participant);
        when(participantService.create(participant)).thenReturn(participant);
        when(modelMapper.map(participant, ParticipantDto.class)).thenReturn(participantDto);

        mockMvc.perform(post("/api/participant/")
                        .content(participantDtoJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void whenCreateParticipant_withInvalidParameters_thenNotFound() throws Exception {
        when(modelMapper.map(participantDto, Participant.class)).thenReturn(participant);
        when(participantService.create(participant)).thenReturn(null);

        mockMvc.perform(post("/api/participant/")
                        .content(participantDtoJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenCreateParticipant_withoutRequestBody_thenBadRequest() throws Exception {
        mockMvc.perform(post("/api/participant/"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenCreateParticipant_withInternalError_thenInternalServerError() throws Exception {
        when(modelMapper.map(participantDto, Participant.class)).thenReturn(participant);
        when(participantService.create(participant)).thenThrow(new RuntimeException());

        mockMvc.perform(post("/api/participant/")
                        .content(participantDtoJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenUpdate_withValidParameters_thenOK() throws Exception {
        when(modelMapper.map(participantDto, Participant.class)).thenReturn(participant);
        when(participantService.update(1L, participant)).thenReturn(participant);
        when(modelMapper.map(participant, ParticipantDto.class)).thenReturn(participantDto);

        mockMvc.perform(put("/api/participant/1")
                        .content(participantDtoJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void whenUpdate_withInvalidParameters_thenNotFound() throws Exception {
        when(modelMapper.map(participantDto, Participant.class)).thenReturn(participant);
        when(participantService.update(1L, participant)).thenReturn(null);

        mockMvc.perform(put("/api/participant/1")
                        .content(participantDtoJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenUpdate_withInternalError_thenInternalServerError() throws Exception {
        when(modelMapper.map(participantDto, Participant.class)).thenReturn(participant);
        when(participantService.update(1L, participant)).thenThrow(new RuntimeException());

        mockMvc.perform(put("/api/participant/1")
                        .content(participantDtoJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenUpdate_withoutRequestBody_thenBadRequest() throws Exception {
        mockMvc.perform(put("/api/participant/1"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenDelete_withValidParameters_thenOK() throws Exception {
        when(participantService.delete(anyLong())).thenReturn(1);

        mockMvc.perform(delete("/api/participant/1"))
                .andExpect(status().isOk());
    }

    @Test
    void whenDelete_withInvalidParameters_thenNotFound() throws Exception {
        when(participantService.delete(anyLong())).thenReturn(0);

        mockMvc.perform(delete("/api/participant/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenDelete_withInternalError_thenInternalServerError() throws Exception {
        when(participantService.delete(anyLong())).thenThrow(new RuntimeException());

        mockMvc.perform(delete("/api/participant/1"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenGetAll_withContent_thenOK() throws Exception {
        when(participantService.getAll()).thenReturn(List.of(new Participant()));

        mockMvc.perform(get("/api/participant/"))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetAll_withoutContent_thenNoContent() throws Exception {
        when(participantService.getAll()).thenReturn(new ArrayList<>());

        mockMvc.perform(get("/api/participant/"))
                .andExpect(status().isNoContent());
    }

    @Test
    void whenGetAll_withInternalError_thenInternalServerError() throws Exception {
        when(participantService.getAll()).thenThrow(new RuntimeException());

        mockMvc.perform(get("/api/participant/"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenGet_withValidParameters_thenOK() throws Exception {
        when(participantService.getById(1L)).thenReturn(new Participant());

        mockMvc.perform(get("/api/participant/1"))
                .andExpect(status().isOk());
    }

    @Test
    void whenGet_withInvalidParameters_thenNotFound() throws Exception {
        when(participantService.getById(1L)).thenReturn(null);

        mockMvc.perform(get("/api/participant/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenGet_withInternalError_thenInternalServerError() throws Exception {
        when(participantService.getById(1L)).thenThrow(new RuntimeException());

        mockMvc.perform(get("/api/participant/1"))
                .andExpect(status().isInternalServerError());
    }
}
