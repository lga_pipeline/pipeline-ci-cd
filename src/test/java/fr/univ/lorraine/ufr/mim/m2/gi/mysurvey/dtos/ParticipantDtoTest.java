package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ParticipantDtoTest {

    ParticipantDto participantDto;

    @BeforeEach
    void setUp() {
        participantDto = new ParticipantDto();
    }

    @Test
    void constructorTest() {
        Assertions.assertNull(participantDto.getParticipantId());
        Assertions.assertNull(participantDto.getNom());
        Assertions.assertNull(participantDto.getPrenom());
    }

    @Test
    void gettersAndSettersTest() {
        participantDto.setParticipantId(1L);
        participantDto.setNom("nom");
        participantDto.setPrenom("prenom");
        Assertions.assertEquals(1L, participantDto.getParticipantId());
        Assertions.assertEquals("nom", participantDto.getNom());
        Assertions.assertEquals("prenom", participantDto.getPrenom());
    }

    @Test
    void equalsTest() {
        Assertions.assertEquals(participantDto, participantDto);

        ParticipantDto participantDto2 = new ParticipantDto();
        participantDto2.setParticipantId(1L);
        Assertions.assertNotEquals(participantDto, participantDto2);

        ParticipantDto participantDto3 = new ParticipantDto();
        participantDto3.setNom("nom");
        Assertions.assertNotEquals(participantDto, participantDto3);

        ParticipantDto participantDto4 = new ParticipantDto();
        participantDto4.setPrenom("prenom");
        Assertions.assertNotEquals(participantDto, participantDto4);

        Assertions.assertNotEquals(null, participantDto);
        Assertions.assertNotEquals(participantDto, new Object());
    }

    @Test
    void hashCodeTest() {
        Assertions.assertEquals(participantDto.hashCode(), participantDto.hashCode());

        ParticipantDto participantDto2 = new ParticipantDto();
        participantDto2.setParticipantId(1L);
        Assertions.assertNotEquals(participantDto.hashCode(), participantDto2.hashCode());

        ParticipantDto participantDto3 = new ParticipantDto();
        participantDto3.setNom("nom");
        Assertions.assertNotEquals(participantDto.hashCode(), participantDto3.hashCode());

        ParticipantDto participantDto4 = new ParticipantDto();
        participantDto4.setPrenom("prenom");
        Assertions.assertNotEquals(participantDto.hashCode(), participantDto4.hashCode());
    }

    @Test
    void toStringTest() {
        Assertions.assertEquals("ParticipantDto{participantId=null, nom='null', prenom='null'}", participantDto.toString());
    }
}
