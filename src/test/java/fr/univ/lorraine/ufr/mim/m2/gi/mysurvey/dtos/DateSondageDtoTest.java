package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

class DateSondageDtoTest {

    private DateSondageDto dateSondageDto;

    @BeforeEach
    void setUp() {
        dateSondageDto = new DateSondageDto();
    }

    @Test
    void constructorTest() {
        Assertions.assertNull(dateSondageDto.getDateSondageId());
        Assertions.assertNull(dateSondageDto.getDate());
    }

    @Test
    void gettersAndSettersTest() {
        Date date = new Date();

        dateSondageDto.setDateSondageId(1L);
        dateSondageDto.setDate(date);

        Assertions.assertEquals(1L, dateSondageDto.getDateSondageId());
        Assertions.assertEquals(date, dateSondageDto.getDate());
    }

    @Test
    void equalsTest() {
        Assertions.assertEquals(dateSondageDto, dateSondageDto);

        DateSondageDto dateSondageDto1 = new DateSondageDto();
        dateSondageDto1.setDateSondageId(2L);
        Assertions.assertNotEquals(dateSondageDto, dateSondageDto1);

        DateSondageDto dateSondageDto2 = new DateSondageDto();
        dateSondageDto2.setDate(new Date());
        Assertions.assertNotEquals(dateSondageDto, dateSondageDto2);

        Assertions.assertNotEquals(null, dateSondageDto);
        Assertions.assertNotEquals(dateSondageDto, new Object());
    }

    @Test
    void hashCodeTest() {
        Assertions.assertEquals(dateSondageDto.hashCode(), dateSondageDto.hashCode());

        DateSondageDto dateSondageDto1 = new DateSondageDto();
        dateSondageDto1.setDateSondageId(2L);
        Assertions.assertNotEquals(dateSondageDto.hashCode(), dateSondageDto1.hashCode());

        DateSondageDto dateSondageDto2 = new DateSondageDto();
        dateSondageDto2.setDate(new Date());
        Assertions.assertNotEquals(dateSondageDto.hashCode(), dateSondageDto2.hashCode());
    }
}
