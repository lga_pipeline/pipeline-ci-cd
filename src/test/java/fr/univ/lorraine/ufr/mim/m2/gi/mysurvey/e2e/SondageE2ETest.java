package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.e2e;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.*;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import java.util.Date;

import static org.hamcrest.Matchers.equalTo;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SondageE2ETest {

    @LocalServerPort
    private Integer port;

    @BeforeEach
    void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
    }

    //////////////CREATE/////////////////

    @Test
    @Order(1)
    void whenCreatingValidParticipant__returnParticipant() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

        ParticipantDto participantDto = new ParticipantDto();
        participantDto.setNom("nom");
        participantDto.setPrenom("prenom");

        String participantDtoJson = objectMapper.writeValueAsString(participantDto);
        RequestSpecification requestParticipant = RestAssured.given();
        requestParticipant.header("Content-Type", "application/json");
        requestParticipant.body(participantDtoJson);
        Response responseParticipant = requestParticipant.post("/api/participant/");

        responseParticipant.then().statusCode(HttpStatus.CREATED.value()).body(
                "participantId", equalTo(1),
                "nom", equalTo("nom"),
                "prenom", equalTo("prenom")
        );
    }

    @Test
    @Order(2)
    void whenCreatingValidSondage__returnSondage() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

        SondageDto sondageDto = new SondageDto();
        sondageDto.setCreateBy(1L);
        sondageDto.setSondageId(1L);
        sondageDto.setNom("titre");
        sondageDto.setDescription("description");
        sondageDto.setCloture(false);

        String sondageDtoJson = objectMapper.writeValueAsString(sondageDto);

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        request.body(sondageDtoJson);
        Response response = request.post("/api/sondage/");

        response.then().statusCode(HttpStatus.CREATED.value()).body(
                "createBy", equalTo(1),
                "sondageId", equalTo(1),
                "nom", equalTo("titre"),
                "description", equalTo("description"),
                "cloture", equalTo(false)
        );
    }

    @Test
    @Order(2)
    void whenCreatingInvalidSondage_returnError400() {
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        Response response = request.post("/api/sondage/");

        response.then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    @Order(2)
    void whenCreatingSondage_withUnknownCreateById_thenReturn404() throws JsonProcessingException {
        SondageDto sondageDto = new SondageDto();
        sondageDto.setCreateBy(999L);

        ObjectMapper objectMapper = new ObjectMapper();
        String sondageDtoJson = objectMapper.writeValueAsString(sondageDto);

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        request.body(sondageDtoJson);
        Response response = request.post("/api/sondage/");

        response.then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    @Order(3)
    void whenCreatingValidCommentaire__returnCommentaire() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

        Integer sondageId = 1;
        CommentaireDto commentaireDto = new CommentaireDto();
        commentaireDto.setParticipant(1L);
        commentaireDto.setCommentaire("Je suis un commentaire");

        String commentaireDtoJson = objectMapper.writeValueAsString(commentaireDto);

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        request.body(commentaireDtoJson);
        Response response = request.post("/api/sondage/" + sondageId + "/commentaires");

        response.then().statusCode(HttpStatus.CREATED.value()).body(
                "commentaireId", equalTo(1),
                "commentaire", equalTo("Je suis un commentaire"),
                "participant", equalTo(1)
        );
    }

    @Test
    @Order(3)
    void whenCreatingInvalidCommentaire_returnError400() {
        Integer sondageId = 1;

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        Response response = request.post("/api/sondage/" + sondageId + "/commentaires");

        response.then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    @Order(3)
    void whenCreatingValidCommentaireWithInvalidSondage_returnError404() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

        Integer sondageId = 999;
        CommentaireDto commentaireDto = new CommentaireDto();
        commentaireDto.setParticipant(1L);
        commentaireDto.setCommentaire("Je suis un commentaire");

        String commentaireDtoJson = objectMapper.writeValueAsString(commentaireDto);

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        request.body(commentaireDtoJson);
        Response response = request.post("/api/sondage/" + sondageId + "/commentaires");

        response.then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    @Order(4)
    void whenCreatingValidDateSondage__returnDateSondage() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

        Integer sondageId = 1;
        Date nowDate = new Date();

        DateSondageDto dateSondageDto = new DateSondageDto();
        dateSondageDto.setDate(nowDate);

        String dateSondageDtoJson = objectMapper.writeValueAsString(dateSondageDto);

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        request.body(dateSondageDtoJson);
        Response response = request.post("/api/sondage/" + sondageId + "/dates");

        response.then().statusCode(HttpStatus.CREATED.value()).body(
                "dateSondageId", equalTo(1)
        );
    }

    @Test
    @Order(5)
    void whenParticipateDateSondage__returnDateSondage() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

        Integer sondageDateId = 1;

        DateSondeeDto dateSondeeDto = new DateSondeeDto();
        dateSondeeDto.setParticipant(1L);
        dateSondeeDto.setChoix("DISPONIBLE");

        String dateSondeeDtoJson = objectMapper.writeValueAsString(dateSondeeDto);

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        request.body(dateSondeeDtoJson);
        Response response = request.post("/api/date/" + sondageDateId + "/participer");

        response.then().statusCode(HttpStatus.CREATED.value()).body(
                "dateSondageId", equalTo(sondageDateId)
        );
    }

    //////////////DELETE/////////////////
    @Test
    @Order(6)
    void whenDeleteSondage__returnOK() throws JsonProcessingException {
        Integer sondageId = 1;

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        Response response = request.delete("/api/sondage/" + sondageId);

        response.then().statusCode(HttpStatus.OK.value());
    }

    @Test
    @Order(7)
    void whenDeleteSondage__returnNotFound() throws JsonProcessingException {
        Integer sondageId = 1;

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        Response response = request.delete("/api/sondage/" + sondageId);

        response.then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    @Order(8)
    void whenGetCommentaires__returnNoContent() throws JsonProcessingException {
        Integer sondageId = 1;

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        Response response = request.get("/api/sondage/" + sondageId + "/commentaires");

        response.then().statusCode(HttpStatus.NO_CONTENT.value());
    }

    @Test
    @Order(8)
    void whenGetDatesSondage__returnNoContent() throws JsonProcessingException {
        Integer sondageId = 1;

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        Response response = request.get("/api/sondage/" + sondageId + "/dates");

        response.then().statusCode(HttpStatus.NO_CONTENT.value());
    }

    @Test
    @Order(8)
    void whenGetParticipant__returnOK() throws JsonProcessingException {
        Integer participantId = 1;

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        Response response = request.get("/api/participant/" + participantId);

        response.then().statusCode(HttpStatus.OK.value());
    }

}
