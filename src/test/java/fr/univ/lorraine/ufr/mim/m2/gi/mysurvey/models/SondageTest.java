package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Date;

class SondageTest {

    private Sondage sondage;
    private Sondage sondage2;
    private Date date;

    @BeforeEach
    void setUp() {
        date = new Date();
        sondage = new Sondage();
        sondage2 = new Sondage(1L, "nom", "description", date, false, null, null, null);
    }

    @Test
    void constructorTest() {
        //Test empty constructor
        Assertions.assertNull(sondage.getSondageId());
        Assertions.assertNull(sondage.getNom());
        Assertions.assertNull(sondage.getDescription());
        Assertions.assertNull(sondage.getFin());
        Assertions.assertNull(sondage.getCloture());
        Assertions.assertTrue(sondage.getCommentaires().isEmpty());
        Assertions.assertTrue(sondage.getDateSondage().isEmpty());
        Assertions.assertNotNull(sondage.getCreateBy());

        //Test constructor with parameters
        Assertions.assertEquals(1L, sondage2.getSondageId());
        Assertions.assertEquals("nom", sondage2.getNom());
        Assertions.assertEquals("description", sondage2.getDescription());
        Assertions.assertEquals(date, sondage2.getFin());
        Assertions.assertFalse(sondage2.getCloture());
        Assertions.assertNull(sondage2.getCommentaires());
        Assertions.assertNull(sondage2.getDateSondage());
        Assertions.assertNull(sondage2.getCreateBy());
    }

    @Test
    void gettersAndSettersTest() {
        sondage.setSondageId(1L);
        sondage.setNom("nom");
        sondage.setDescription("description");
        sondage.setFin(date);
        sondage.setCloture(false);
        sondage.setCommentaires(null);
        sondage.setDateSondage(null);
        sondage.setCreateBy(null);

        Assertions.assertEquals(1L, sondage.getSondageId());
        Assertions.assertEquals("nom", sondage.getNom());
        Assertions.assertEquals("description", sondage.getDescription());
        Assertions.assertEquals(date, sondage.getFin());
        Assertions.assertFalse(sondage.getCloture());
        Assertions.assertNull(sondage.getCommentaires());
        Assertions.assertNull(sondage.getDateSondage());
        Assertions.assertNull(sondage.getCreateBy());
    }

    @Test
    void testFieldsAnnotations() throws NoSuchFieldException {
        Field classMemberField = Sondage.class.getDeclaredField("sondageId");
        Assertions.assertTrue(classMemberField.isAnnotationPresent(jakarta.persistence.GeneratedValue.class));
        Assertions.assertTrue(classMemberField.isAnnotationPresent(jakarta.persistence.Id.class));

        Field classMemberField2 = Sondage.class.getDeclaredField("commentaires");
        Assertions.assertTrue(classMemberField2.isAnnotationPresent(jakarta.persistence.OneToMany.class));
        Annotation[] annotations = classMemberField2.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof jakarta.persistence.OneToMany) {
                jakarta.persistence.OneToMany oneToMany = (jakarta.persistence.OneToMany) annotation;
                Assertions.assertEquals(jakarta.persistence.CascadeType.ALL, oneToMany.cascade()[0]);
                Assertions.assertEquals("sondage", oneToMany.mappedBy());
            }
        }

        Field classMemberField3 = Sondage.class.getDeclaredField("dateSondage");
        Assertions.assertTrue(classMemberField3.isAnnotationPresent(jakarta.persistence.OneToMany.class));
        Annotation[] annotations2 = classMemberField3.getAnnotations();
        for (Annotation annotation : annotations2) {
            if (annotation instanceof jakarta.persistence.OneToMany) {
                jakarta.persistence.OneToMany oneToMany = (jakarta.persistence.OneToMany) annotation;
                Assertions.assertEquals(jakarta.persistence.CascadeType.ALL, oneToMany.cascade()[0]);
                Assertions.assertEquals("sondage", oneToMany.mappedBy());
            }
        }

        Field classMemberField4 = Sondage.class.getDeclaredField("createBy");
        Assertions.assertTrue(classMemberField4.isAnnotationPresent(jakarta.persistence.ManyToOne.class));
        Annotation[] annotations3 = classMemberField4.getAnnotations();
        for (Annotation annotation : annotations3) {
            if (annotation instanceof jakarta.persistence.ManyToOne) {
                jakarta.persistence.ManyToOne manyToOne = (jakarta.persistence.ManyToOne) annotation;
                Assertions.assertEquals(jakarta.persistence.FetchType.LAZY, manyToOne.fetch());
            }
        }
    }

}
