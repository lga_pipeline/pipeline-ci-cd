import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.MySurveyApplication;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

// Test class added ONLY to cover main() invocation not covered by application tests.
class MySurveyApplicationTest {
    @Test
    void main() {
        Assertions.assertAll(() -> {
            MySurveyApplication.main(new String[]{});
        });
    }
}