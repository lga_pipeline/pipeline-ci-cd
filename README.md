Pour la base de données:

I/ Via docker
1/ Windows/Linux

- CMD : ``docker pull postgre``
- CMD : ``docker run --name pipeline-project-postgre -e POSTGRES_PASSWORD=admin -p 5432:5432 -d postgres``

Pour accéder à l'application:

``http://141.94.27.58:8081`` 

Pour accéder à la documentation swagger:

``http://141.94.27.58:8081/swagger-ui/index.html''

Notre pipeline de CI/CD comporte 7 étapes:

- Compile: Compilation du code source
- Test: Exécution des tests grâce à Jacoco, avec un minimum de 80% de couverture de code
- Sonar: Analyse du code source grâce à SonarCloud
- Vulnerability: Analyse des vulnérabilités des dépendances grâce à owasp-dependency-check
- Format: Application du formatage du code source
- Build: Construction de l'application
- Deploy: Déploiement de l'application